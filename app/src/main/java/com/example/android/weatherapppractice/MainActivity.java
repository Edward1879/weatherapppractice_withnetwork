package com.example.android.weatherapppractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private WeatherListAdapter weatherListAdapter;
    private ArrayList<Weather> weatherArrayList;
    private RequestQueue requestQueue;
    private String baseURL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
    private String API_KEY = "58bf3c983d89485e90d1a287014d0a08";
    private String CITY_NAME = "HongKong,LaiKing";
    private String UNITS = "metric";
    private String CNT = "16";

    private String REQUEST_URL = baseURL+"q="+CITY_NAME+"&cnt="+CNT+"&appid="+API_KEY+"&units="+UNITS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weatherArrayList = new ArrayList<>();

        listView = (ListView) findViewById(R.id.listView_main);
        Log.d("sunny", "REQUEST_URL: "+ REQUEST_URL);
        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, REQUEST_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("sunny", "response: "+ response.toString());
                //got the JSON return here and extract the data from here
                try {
                    //get the list of weather data
                    JSONArray jsonArray = response.getJSONArray("list");
                    Log.d("sunny", jsonArray.toString());

                    //use a for loop to extract the data needed and store them into different Weather object
                    for (int i = 0; i<jsonArray.length();i++){
                        JSONObject currentJSONObject = jsonArray.getJSONObject(i);
                        String date = currentJSONObject.getString("dt");
                        String temp = currentJSONObject.getJSONObject("temp").getString("day");
                        String description = currentJSONObject.getJSONArray("weather").getJSONObject(0).getString("main");

                        Weather weather = new Weather(date,temp,description);

                        weatherArrayList.add(weather);

                    }
                    weatherListAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("sunny", "Error: "+error);

            }
        }) ;

        requestQueue.add(jsonObjectRequest);

//        Weather weather1 = new Weather("1-Nov","25°","Sunny");
//        Weather weather2 = new Weather("2-Nov","26°","Cloudy");
//        weatherArrayList.add(weather1);
//        weatherArrayList.add(weather2);
        weatherListAdapter = new WeatherListAdapter(weatherArrayList,this);
        listView.setAdapter(weatherListAdapter);


//        System.out.println(weather1.getDate());
//        weather1.setDate("2-Nov");
//        System.out.println(weather1.getDate());


    }
}
